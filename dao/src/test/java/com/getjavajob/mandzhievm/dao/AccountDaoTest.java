package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ComponentScan
//@SpringBootTest
@DataJpaTest
public class AccountDaoTest {
    @Autowired
    private AccountDao dao;
//    private EntityManagerFactory entityManagerFactory;
    @Autowired
    private TestEntityManager em;

    @Before
    public void setUp() {
        Account account = new Account("bruce", "willis", "petrovich", LocalDate.now(),
                "", "", "buba", "", "", "", "");
        Account accountOne = new Account("bruce1", "willis1", "petrovich1", LocalDate.now(),
                "", "", "", "", "", "", "");
        em.persist(account);
        em.persist(accountOne);
        System.out.println(account.getId());
        System.out.println(accountOne.getId());
    }

    @Test
    @Transactional
    public void getIdTest() {
        Account expected = dao.findById(10);
        assertEquals(expected.getLastName(), "bruce");
    }

    @Test
    @Transactional
    public void getAllTest() {
        List<Account> accounts = dao.getAll();
        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    public void create() {
        Account account2 = new Account("bruce2", "willis2", "petrovich2", LocalDate.now(),
                "", "", "", "", "", "", "");
        dao.save(account2);
        Account actual = dao.findById(3);
        assertEquals(account2.getLastName(), actual.getLastName());
    }

//    @Test
//    @Transactional
//    public void deleteById() throws Exception {
//        dao.deleteById(1);
//        Account actual = dao.getId(3);
//        assertNull(actual);
//    }

//    @Test
//    @Transactional
//    public void updateById() throws Exception {
//        Account account = new Account();
//        account.setLastName("bruce5");
//        account.setFirstName("willis5");
//        account.setMiddleName("petrovich");
//        account.setId(1);
//        dao.updateById(account);
//        assertEquals("willis5", dao.getId(1).getFirstName());
//    }

    @Test
    @Transactional
    public void findEmail() {
        Account account = dao.findEmail("buba");
        assertEquals("willis", account.getFirstName());
    }

    @Test
    @Transactional
    public void doSearch() {
        List<Account> accounts = dao.doSearch("b", 1, 2);
        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    public void countFromAccountSearch() {
        assertEquals(Long.valueOf(2), dao.countFromAccountSearch("b"));
    }
}