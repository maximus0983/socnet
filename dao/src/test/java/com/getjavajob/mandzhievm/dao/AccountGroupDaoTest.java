package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.AccountGroup;
import com.getjavajob.mandzhievm.common.Group;
import com.getjavajob.mandzhievm.common.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AccountGroupDaoTest {

    @Autowired
    private AccountGroupDao dao;
    @Autowired
    private TestEntityManager em;

    @Before
    public void setUp() {
        Account account = new Account("bruce", "willis", "petrovich", LocalDate.now(),
                "", "", "buba", "", "", "", "");
        Account accountOne = new Account("bruce1", "willis1", "petrovich1", LocalDate.now(),
                "", "", "", "", "", "", "");
        em.persist(account);
        em.persist(accountOne);
        System.out.println("acc1: " + account.getId());
        System.out.println("acc2: " + accountOne.getId());
        Group group1 = new Group("gr1");
        Group group2 = new Group("gr2");
        em.persist(group1);
        em.persist(group2);
        System.out.println("gr1: " + group1.getIdGroup());
        System.out.println("gr2: " + group2.getIdGroup());
        AccountGroup accountGroup = new AccountGroup(account, group1);
        accountGroup.setStatus(1);
        accountGroup.setUser(User.user);
        em.persist(accountGroup);
        account.addGroup(group1, accountGroup);
        AccountGroup accountGroup1 = new AccountGroup(accountOne, group1);
        accountGroup1.setUser(User.user);
        accountGroup1.setStatus(1);
        em.persist(accountGroup1);
        AccountGroup accountGroup2 = new AccountGroup(account, group2);
        accountGroup2.setStatus(1);
        accountGroup2.setUser(User.user);
        em.persist(accountGroup2);
        AccountGroup accountGroup3 = new AccountGroup(accountOne, group2);
        accountGroup3.setUser(User.user);
        accountGroup3.setStatus(0);
        em.persist(accountGroup3);
        Group group3 = new Group("gr3");
        em.persist(group3);
    }

    @Test
    public void groupRequestSent() {
        int status = dao.groupRequestSent(1, 1);
        assertEquals(1, status);
    }

    @Test
    public void getUser() {
        String user = dao.getUser(1, 3);
        assertEquals("", user);
    }

    @Test
    public void getAll() {
        List<Account> actual = dao.getAll(1, User.user);
        assertEquals(Arrays.asList(14, 15), Arrays.asList(actual.get(0).getId(), actual.get(1).getId()));
    }

    @Test
    public void getGroupsByAccountId() {
        List<Integer> actual = dao.getGroupsByAccountId(1);
        assertEquals(Arrays.asList(1, 2), actual);
    }

    @Test
    public void groupRequest() {
        dao.groupRequest(2, 3, User.user, 1);
        assertEquals(1, dao.getAll(3, User.user).size());
    }

    @Test
    public void acceptGroupRequest() {
        dao.acceptGroupRequest(0, 1, 1);
        assertEquals(0, dao.groupRequestSent(1, 1));
    }

    @Test
    public void pendingGroupRequest() {
        List<Account> actual = dao.pendingGroupRequest(2);
        assertEquals(2, actual.get(0).getId().intValue());
    }

    @Test
    public void createAdmin() {
        dao.createAdmin(1, 1);
        assertEquals("admin", dao.getUser(1, 1));
    }
}