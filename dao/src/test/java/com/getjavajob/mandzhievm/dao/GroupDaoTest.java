package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Group;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GroupDaoTest {
    @Autowired
    GroupDao dao;
    @Autowired
    private TestEntityManager em;

    @Before
    public void setUp() {
        Group group = new Group("getjavajob");
        group.setCreateDate(LocalDate.now());
        System.out.println(group.getIdGroup());
        em.persist(group);
        Group group1 = new Group("getjavajob1");
        group.setCreateDate(LocalDate.now());
        System.out.println(group.getIdGroup());
        em.persist(group1);
    }

    @Test
    public void getById() {
        Group expected = dao.getById(1);
        assertEquals("getjavajob", expected.getNameGroup());
    }

    @Test
    public void getAll() {
        List<Group> groups = dao.getAll();
        assertEquals(2, groups.size());
    }

    @Test
    public void getByName() {
        assertEquals("getjavajob", dao.getByName("getjavajob").getNameGroup());
    }

    @Test
    public void create() {
        Group group = new Group();
        group.setNameGroup("1v");
        group.setCreateDate(LocalDate.now());
        dao.create(group);
        assertEquals("1v", dao.getByName("1v").getNameGroup());
    }

    @Test
    public void deleteById() {
        dao.deleteById(1);
        Group group = dao.getById(1);
        assertNull(group);
    }

    @Test
    public void updateById() {
        Group group = new Group();
        group.setNameGroup("1v");
        dao.updateById(group, 1);
        assertEquals("1v", dao.getById(1).getNameGroup());
    }
}