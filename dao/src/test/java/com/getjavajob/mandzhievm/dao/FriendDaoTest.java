package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Friends;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FriendDaoTest {
    @Autowired
    private FriendDao dao;
    @Autowired
    TestEntityManager em;

    @Before
    public void setUp() {
        Friends friends = new Friends(1, 2);
        friends.setStatus(1);
        friends.setAction(1);
        em.persist(friends);
        Friends friends1 = new Friends(2, 3);
        friends1.setStatus(1);
        friends1.setAction(2);
        em.persist(friends1);
        Friends friends2 = new Friends(3, 4);
        friends2.setStatus(1);
        friends2.setAction(3);
        em.persist(friends2);
    }

    @Test
    public void getAll() {
        List<Integer> actual = dao.getAll(2);
        assertEquals(Arrays.asList(1, 3), Arrays.asList(actual.get(0), actual.get(1)));
    }

    @Test
    public void friendRequest() {
        dao.friendRequest(1, 3, 1);
        assertEquals(1, (int) dao.pendingRequest(3).get(0));
    }

    @Test
    public void acceptFriendRequest() {
        dao.friendRequest(4, 5, 5);
        dao.acceptFriendRequest(1, 4, 4, 5);
        assertEquals(1, dao.friendRequestSent(4, 5));
    }

    @Test
    public void pendingRequest() {
        dao.friendRequest(1, 3, 3);
        assertEquals(3, (int) dao.pendingRequest(1).get(0));
    }

    @Test
    public void friendRequestSent() {
        assertEquals(1, dao.friendRequestSent(2, 1));
    }
}