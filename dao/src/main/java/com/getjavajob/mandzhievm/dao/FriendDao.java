package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Friends;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FriendDao {
    private static final Logger logger = LoggerFactory.getLogger(FriendDao.class);

    @PersistenceContext
    private EntityManager em;

    public List<Integer> getAll(int accOne) {
        Query query = em.createQuery(
                "from Friends f WHERE (f.accountOne = :accountId OR f.accountTwo = :accountId) AND status = 1")
                .setParameter("accountId", accOne);
        List<Friends> friends = query.getResultList(); //unchecked
        List<Integer> accounts = new ArrayList<>();
        for (Friends friend : friends) {
            int account = friend.getAccountOne();
            if (account != accOne) {
                accounts.add(account);
            } else {
                accounts.add(friend.getAccountTwo());
            }
        }
        logger.info("Friends list size is " + accounts.size());
        return accounts;
    }

    public boolean friendRequest(int accOneId, int accTwoId, int action) {
        int userOne;
        int userTwo;
        if (accOneId < accTwoId) {
            userOne = accOneId;
            userTwo = accTwoId;
        } else {
            userOne = accTwoId;
            userTwo = accOneId;
        }
        Friends friends = new Friends(userOne, userTwo);
        friends.setAction(action);
        friends.setStatus(0);
        em.persist(friends);
        return true;
    }

    public boolean acceptFriendRequest(int status, int action, int accOneId, int accTwoId) {
        int userOne;
        int userTwo;
        if (accOneId < accTwoId) {
            userOne = accOneId;
            userTwo = accTwoId;
        } else {
            userOne = accTwoId;
            userTwo = accOneId;
        }
        Query query = em.createQuery(
                "from Friends f WHERE f.accountOne = :userOne and f.accountTwo = :userTwo")
                .setParameter("userOne", userOne).setParameter("userTwo", userTwo);
        Friends friends = (Friends) query.getResultList().get(0);
        em.persist(friends);
        friends.setStatus(status);
        friends.setAction(action);
        return true;
    }

    public List<Integer> pendingRequest(int acc) {
        Query query = em.createQuery(
                "from Friends f where (f.accountOne = :accountId or f.accountTwo = :accountId) and status = 0 " +
                        "AND action != :accountId").setParameter("accountId", acc);
        List<Friends> friends = query.getResultList(); //unchecked
        List<Integer> accounts = new ArrayList<>();
        for (Friends friend : friends) {
            Integer account = friend.getAccountOne();
            if (account != acc) {
                accounts.add(account);
            } else {
                accounts.add(friend.getAccountTwo());
            }
        }
        logger.info("Pending request size " + accounts.size());
        return accounts;
    }

    public int friendRequestSent(int accOneId, int accTwoId) {
        int userOne;
        int userTwo;
        if (accOneId < accTwoId) {
            userOne = accOneId;
            userTwo = accTwoId;
        } else {
            userOne = accTwoId;
            userTwo = accOneId;
        }
        Query query = em.createQuery(
                "select status from Friends f WHERE f.accountOne = :userOne and f.accountTwo = :userTwo")
                .setParameter("userOne", userOne).setParameter("userTwo", userTwo);
        List<Integer> status = query.getResultList();
        if (status.size() > 0) return status.get(0);
        else return -1;
    }
}