package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDao {
    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    @Autowired
    private AccountRepository accountRepository;

    public Account findById(int id) {
        return accountRepository.findFirstById(id);
    }

    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public boolean deleteAccount(int id) {
        accountRepository.deleteById(id);
        return true;
    }

    public Account findEmail(String email) {
        return accountRepository.findByEmail(email);
    }

    public List<Account> doSearch(String search, int page, int pageLimit) {
        Pageable pageable = PageRequest.of(page - 1, pageLimit);
        List<Account> accounts = accountRepository
                .findAllByFirstNameContainingOrLastNameContainingAllIgnoreCase(search, search, pageable);
        logger.info("List of accounts size {}", accounts.size());
        return accounts;
    }

    public Long countFromAccountSearch(String search) {
        return accountRepository.countByLastNameIsContainingOrFirstNameIsContainingAllIgnoreCase(search, search);
    }
}