package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.AccountGroup;
import com.getjavajob.mandzhievm.common.Group;
import com.getjavajob.mandzhievm.common.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountGroupDao {
    private static final Logger logger = LoggerFactory.getLogger(AccountGroupDao.class);

    @PersistenceContext
    private EntityManager em;

    public int groupRequestSent(int groupId, int accountId) {
        Query query = em.createQuery(
                "select a.status from AccountGroup a where a.group.id=?1 and a.account.id=?2 and " +
                        "a.user = user").setParameter(1, groupId).setParameter(2, accountId);
        List<Integer> status = (List<Integer>) query.getResultList();
        if (status.size() > 0) return status.get(0);
        else return -1;
    }

    public String getUser(int groupId, int accountId) {
        Query query = em.createQuery("select a.user from AccountGroup a where a.account.id =?1 and a.group.id=?2")
                .setParameter(1, accountId).setParameter(2, groupId);
        if (query.getResultList().size() > 0) {
            User user = (User) query.getResultList().get(0);
            return user.name();
        } else {
            return "";
        }
    }

    public List<Account> getAll(int groupId, User user) {
        Query query = em.createQuery(
                "from AccountGroup a where a.group.id = ?1 and a.user = ?2 and a.status = ?3")
                .setParameter(1, groupId).setParameter(2, user).setParameter(3, 1);
        List<Account> accounts = new ArrayList<>();
        List<AccountGroup> accountGroups = query.getResultList();
        for (AccountGroup accountGroup : accountGroups) {
            accounts.add(accountGroup.getAccount());
        }
        return accounts;
    }

    public List<Integer> getGroupsByAccountId(int accountId) {
        Query query = em.createQuery("select a.group.id from AccountGroup a where a.account.id = :accountId")
                .setParameter("accountId", accountId);
        return query.getResultList();
    }

    public boolean groupRequest(int accId, int groupId, User user, int status) {
        Account account = em.find(Account.class, accId);
        Group group = em.find(Group.class, groupId);
        AccountGroup accountGroup = new AccountGroup(account, group);
        accountGroup.setUser(user);
        accountGroup.setStatus(status);
        account.addGroup(group, accountGroup);
        em.persist(account);
        return true;
    }

    public boolean delete(int accountId, int groupId) {
        Account account = em.find(Account.class, accountId);
        Group group = em.find(Group.class, groupId);
        account.removeGroup(group);
        em.persist(account);
        AccountGroup accountGroup = (AccountGroup) em.createQuery(
                "select a from AccountGroup a where a.account.id =?1 and a.group.id=?2")
                .setParameter(1, accountId).setParameter(2, groupId).getResultList().get(0);
        em.remove(accountGroup);
        return true;
    }

    public boolean acceptGroupRequest(int status, int idaccount, int groupId) {
        Account account = em.find(Account.class, idaccount);
        List<AccountGroup> accountGroups = account.getGroups();
        AccountGroup accountGroup = (AccountGroup) em.createQuery(
                "from AccountGroup a where a.account.id =?1 and a.group.id=?2")
                .setParameter(1, idaccount).setParameter(2, groupId).getResultList().get(0);
        for (AccountGroup accountGroup1 : accountGroups) {
            if (accountGroup1.equals(accountGroup)) {
                em.persist(accountGroup1);
                accountGroup1.setStatus(status);
            }
        }
        em.persist(account);
        return true;
    }

    public List<Account> pendingGroupRequest(int groupId) {
        Query query = em.createQuery(
                "from AccountGroup a where a.group.id = ?1 and status = 0 ")
                .setParameter(1, groupId);
        List<Account> accounts = new ArrayList<>();
        List<AccountGroup> accountGroups = query.getResultList();
        for (AccountGroup accountGroup : accountGroups) {
            accounts.add(accountGroup.getAccount());
        }
        return accounts;
    }

    public AccountGroup createAdmin(int accountId, int groupId) {
        Account account = em.find(Account.class, accountId);
        em.persist(account);
        AccountGroup accountGroup = (AccountGroup) em.createQuery(
                "select a from AccountGroup a where a.account.id =?1 and a.group.id=?2")
                .setParameter(1, accountId).setParameter(2, groupId).getResultList().get(0);
        List<AccountGroup> accountGroups = account.getGroups();
        for (AccountGroup accountGroup1 : accountGroups) {
            if (accountGroup1.equals(accountGroup)) {
                accountGroup1.setUser(User.admin);
                accountGroup = accountGroup1;
            }
        }
        logger.info("Admin created as id " + account.getId());
        return accountGroup;
    }
}