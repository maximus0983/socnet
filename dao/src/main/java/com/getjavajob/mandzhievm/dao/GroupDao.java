package com.getjavajob.mandzhievm.dao;

import com.getjavajob.mandzhievm.common.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class GroupDao {
    private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

    @PersistenceContext
    private EntityManager em;

    public Group getById(int groupId) {
        Group group = em.find(Group.class, groupId);
        if (group != null) {
            logger.info("group find with id: {}", group.getIdGroup());
        }
        return group;
    }

    public List<Group> getAll() {
        Query query = em.createQuery("from Group", Group.class);
        return (List<Group>) query.getResultList();
    }

    public Group getByName(String nameGroup) {
        Query query = em.createQuery(
                "from Group g where g.nameGroup = :name").setParameter("name", nameGroup);
        return (Group) query.getResultList().get(0);
    }

    public void create(Group group) {
        em.persist(group);
        logger.info("group saved with id = {}", group.getIdGroup());
    }

    public boolean deleteById(int groupId) {
        Group group = em.find(Group.class, groupId);
        em.remove(group);
        return true;
    }

    public boolean updateById(Group group, int groupId) {
        Group updateGroup = em.find(Group.class, groupId);
        em.persist(updateGroup);
        updateGroup.setNameGroup(group.getNameGroup());
        updateGroup.setPhoto(group.getPhoto());
        updateGroup.setInfo(group.getInfo());
        updateGroup.getAccounts();
        return true;
    }


    public List<Group> doSearch(String search, int page, int pageLimit) {
        Query query = em.createQuery(
                "from Group g where g.nameGroup like :search", Group.class).setParameter(
                "search", "%" + search + "%")
                .setFirstResult((page - 1) * pageLimit).setMaxResults(pageLimit);
        return query.getResultList();
    }

    public Long countFromGroupSearch(String search) {
        return (Long) em.createQuery(
                "select count(*) from Group g where g.nameGroup like :search")
                .setParameter("search", "%" + search + "%")
                .getSingleResult();
    }
}