package com.getjavajob.mandzhievm.web.controllers;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.service.AccountService;
import com.getjavajob.mandzhievm.common.Message;
import com.getjavajob.mandzhievm.common.MessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private AccountService accountService;

    @MessageMapping("/greeting")
    public void send(MessageDTO messageDTO, Principal principal) {
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        messageDTO.setTime(time);
        Account from=accountService.findEmail(messageDTO.getFrom());
        Account to=accountService.findEmail(messageDTO.getTo());
        Message message=new Message(from,messageDTO.getText(),messageDTO.getTime(),to,"chat");
        accountService.save(message);
        logger.debug("{} : {}", messageDTO.getFrom(), messageDTO.getText());
        simpMessagingTemplate.convertAndSendToUser(messageDTO.getTo(), "/queue/reply", messageDTO);
        simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", messageDTO);
    }

    @RequestMapping("/message")
    public ModelAndView goMessage(@RequestParam("id") int id, @SessionAttribute("account") Account currentUser) {
        ModelAndView modelAndView = new ModelAndView("messages");
        Account account = accountService.getById(id);
        String to = account.getEmail();
        String name = account.getFirstName();
        List<Message> messages = accountService.getMessages(currentUser, account,"chat");
        modelAndView.addObject("to", to);
        modelAndView.addObject("toName", name);
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("secondAccount", account);
        return modelAndView;
    }
}