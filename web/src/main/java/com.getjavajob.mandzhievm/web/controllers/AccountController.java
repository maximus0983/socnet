package com.getjavajob.mandzhievm.web.controllers;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.Message;
import com.getjavajob.mandzhievm.service.AccountService;
import com.getjavajob.mandzhievm.service.CustomUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(value = "/wallMessage")
    public String wallMessage(@RequestParam("id") int id, @RequestParam("wallMessage") String wallMessage,
                              @SessionAttribute("account") Account currentUser) {
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        Account to = accountService.getById(id);
        Message message = new Message(currentUser, wallMessage, time, to, "wall");
        accountService.save(message);
        return "redirect:/account?id=" + id;
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView viewAccount(@RequestParam("id") int id, @SessionAttribute("account") Account currentUser) {
        Account account = accountService.getById(id);
        List<Message> messages = accountService.getWallMessages(account, "wall");
        logger.debug("in account controller");
        ModelAndView modelAndView = new ModelAndView("accountPage");
        modelAndView.addObject("account", account);
        modelAndView.addObject("messages", messages);
        int status = accountService.friendRequestSent(currentUser.getId(), id);
        logger.info("status {} {}", status, currentUser.getId());
        modelAndView.addObject("status", status);
        return modelAndView;
    }

    @RequestMapping(value = "/image", method = RequestMethod.GET)
    public void viewImage(@RequestParam("id") int id, HttpServletResponse resp) throws IOException {
        Account account = accountService.getById(id);
        resp.setContentType("image/jpeg");
        resp.getOutputStream().write(account.getPhoto());
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam("search") String search) {
        ModelAndView modelAndView = new ModelAndView("resultSearch");
        modelAndView.addObject("search", search);
        modelAndView.addObject("totalAccount", accountService.getCountFromAccountSearch(search));
        modelAndView.addObject("totalGroup", accountService.getCountFromGroupSearch(search));
        return modelAndView;
    }

    @RequestMapping(value = "/paginationAjaxSearch", method = RequestMethod.GET)
    @ResponseBody
    public List<Object> getAjaxSearch(final @RequestParam("search") String search, @RequestParam("page") int page,
                                      @RequestParam("pageLimit") int pageLimit) {
        logger.info("In Pagination search");
        List<Object> result = accountService.paginationSearch(search, page, pageLimit);
        logger.info("Result size: {} ", result.size());
        return result;
    }

    @RequestMapping(value = "/")
    public String login() {
        return "index";
    }

    @RequestMapping(value = "/registration")
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping(value = "/update")
    public String update() {
        return "update";
    }

    @RequestMapping(value = "/registr", method = RequestMethod.POST)
    public ModelAndView registrationAccount(@ModelAttribute Account regAccount, HttpServletRequest req,
                                            @RequestParam("file") MultipartFile file, @RequestParam("accountBirthDay") String birthDay,
                                            HttpServletRequest request, HttpServletResponse resp) throws IOException {
        ModelAndView modelAndView = null;
        if (accountService.findEmail(regAccount.getEmail()) == null) {
            regAccount.setRegDate(LocalDate.now());
            if (file.getSize() != 0) {
                byte[] picture = file.getBytes();
                regAccount.setPhoto(picture);
            }
            if (!birthDay.isEmpty()) {
                regAccount.setBirthDay(LocalDate.parse(birthDay));
            }
            String regPassword = regAccount.getPassword();
            String password = passwordEncoder.encode(regAccount.getPassword());
            regAccount.setPassword(password);
            regAccount.setRole(2);
            String[] inputPhones = req.getParameterValues("homePhone");
            Account account = accountService.create(regAccount, inputPhones);
            HttpSession session = req.getSession();
            session.setAttribute("account", account);
            logger.debug("Successfull for {} ", account.getFirstName());
            CustomUserDetailsService userDetailsService = new CustomUserDetailsService();
            User user = new User(regAccount.getEmail(),
                    account.getPassword(),
                    true,
                    true,
                    true,
                    true,
                    userDetailsService.getAuthorities(regAccount.getRole()));
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
                    regPassword, user.getAuthorities());
            authenticationManager.authenticate(authentication);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            Authentication auth = securityContext.getAuthentication();
            logger.debug("reg controller {} ", auth);//todo placeholder
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            resp.sendRedirect(request.getContextPath() + "/account?id=" + account.getId());
        } else {
            modelAndView = new ModelAndView("registration");
            modelAndView.addObject("message", "Sorry, Email already exist!");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public String updateAccount(@ModelAttribute Account account, @RequestParam("file") MultipartFile file,
                                HttpServletRequest req, @RequestParam("accountBirthDay") String birthDay,
                                HttpSession session, @SessionAttribute("account") Account currentUser) throws IOException {
        if (file.getSize() != 0) {
            logger.info("File size: {} byte", file.getBytes());
            byte[] picture = file.getBytes();
            account.setPhoto(picture);
        } else account.setPhoto(currentUser.getPhoto());
        if (!birthDay.isEmpty()) {
            account.setBirthDay(LocalDate.parse(birthDay));
        }
        account.setRole(2);
        String[] inputPhones = req.getParameterValues("homePhone");
        accountService.update(account, inputPhones);
        session.setAttribute("account", account);
        return "redirect:/account?id=" + account.getId();
    }

    @RequestMapping(value = "/quickSearch", method = RequestMethod.GET)
    @ResponseBody
    public List<Object> getSearch(final @RequestParam("search") String search) {
        return accountService.paginationSearch(search, 1, 3);
    }

    @ExceptionHandler({IOException.class, JAXBException.class})
    public ModelAndView handlerException(Exception e) {
        logger.error(e.getMessage());
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("error", e.getMessage());
        return modelAndView;
    }

    @RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
    public String deleteAccount(int id, @SessionAttribute("account") Account currentUser) {
        accountService.deleteAccount(id);
        return "redirect:/account?id=" + currentUser.getId();
    }

    @RequestMapping(value = "/writeAccount", produces = "application/xml")
    @ResponseBody
    public Account writeAccountToXML(@SessionAttribute("account") Account account, HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"account.xml\"");
        return account;
    }

    @RequestMapping(value = "/readFile", method = RequestMethod.POST)
    public ModelAndView readAccountFromXML(@RequestParam("file") MultipartFile file) throws JAXBException, IOException {
        ModelAndView modelAndView = new ModelAndView("update");
        JAXBContext context = JAXBContext.newInstance(Account.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        String content = new String(file.getBytes(), StandardCharsets.UTF_8);
        StringReader reader = new StringReader(content);
        Account account = (Account) unmarshaller.unmarshal(reader);
        modelAndView.addObject(account);
        return modelAndView;
    }
}