package com.getjavajob.mandzhievm.web.controllers;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.service.AccountService;
import com.getjavajob.mandzhievm.common.Group;
import com.getjavajob.mandzhievm.common.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller
public class GroupController {
    @Autowired
    AccountService accountService;

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute Group group, @SessionAttribute("account") Account account,
                              @RequestParam("file") MultipartFile file) throws IOException {
        group.setAccountId(account.getId());
        group.setCreateDate(LocalDate.now());
        if (file.getSize() != 0) {
            group.setPhoto(file.getBytes());
        }
        accountService.create(group);
        int id = accountService.getGroupByName(group.getNameGroup());
        return "redirect:/group?id=" + id;
    }

    @RequestMapping(value = "/group")
    public ModelAndView group(@RequestParam("id") int groupId, @SessionAttribute("account") Account account) {
        Group group = accountService.getGroupById(groupId);
        ModelAndView modelAndView = new ModelAndView("group");
        modelAndView.addObject("group", group);
        List<Account> pendingList = accountService.pendingGroupRequest(group.getIdGroup());
        modelAndView.addObject("pendingList", pendingList);
        int accountId = account.getId();
        String user = accountService.getUserById(groupId, accountId);
        System.out.println(user);
        modelAndView.addObject("user", user);
        List<Account> admins = accountService.groupMembers(group.getIdGroup(), User.admin);
        modelAndView.addObject("admins", admins);
        int status = accountService.groupRequestSent(groupId, accountId);
        modelAndView.addObject("status", status);
        System.out.println(status);
        return modelAndView;
    }

    @RequestMapping(value = "/imageGroup", method = RequestMethod.GET)
    public void imageGroup(@RequestParam("id") int id, HttpServletResponse resp) throws IOException {
        Group group = accountService.getGroupById(id);
        resp.setContentType("image/jpeg");
        resp.getOutputStream().write(group.getPhoto());
    }

    @RequestMapping(value = "/updateGroupPage", method = RequestMethod.GET)
    public ModelAndView viewUpdateGroup(@RequestParam("id") int id) {
        Group group = accountService.getGroupById(id);
        ModelAndView modelAndView = new ModelAndView("UpdateGroup");
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @RequestMapping(value = "/updateGroup")
    public String updateGroup(@ModelAttribute Group group, @RequestParam("file") MultipartFile file) throws IOException {
        if (file.getSize() != 0) {
            byte[] picture = file.getBytes();
            group.setPhoto(picture);
        } else group.setPhoto(accountService.getGroupById(group.getIdGroup()).getPhoto());
        accountService.update(group);
        int id = accountService.getGroupByName(group.getNameGroup());
        return "redirect:/group?id=" + id;
    }

    @RequestMapping(value = "/deleteGroup")
    public String deleteGroup(@RequestParam("id") int groupId, @SessionAttribute("account") Account account) {
        accountService.deleteGroup(groupId);
        return "redirect:/account?id=" + account.getId();
    }

    @RequestMapping(value = "/members")
    public ModelAndView members(@RequestParam("id") int groupId, @SessionAttribute("account") Account account) {
        ModelAndView modelAndView = new ModelAndView("groupMembers");
        modelAndView.addObject("groupId", groupId);
        List<Account> members = accountService.groupMembers(groupId, User.user);
        modelAndView.addObject("members", members);
        List<Account> pendingList = accountService.pendingGroupRequest(groupId);
        modelAndView.addObject("pendingList", pendingList);
        int accountId = account.getId();
        String user = accountService.getUserById(groupId, accountId);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping(value = "/groupRequest")
    public String groupRequest(@RequestParam("id") int groupId, @SessionAttribute("account") Account account) {
        int accountId = account.getId();
        accountService.groupRequest(accountId, groupId, User.user, 0);
        return "redirect:/group?id=" + groupId;
    }

    @RequestMapping(value = "/deleteFromGroup")
    public String deleteFromGroup(@RequestParam("groupId") int groupId, @RequestParam("accountId") int accountId) {
        accountService.deleteFromGroupByAccountId(accountId, groupId);
        return "redirect:/group?id=" + groupId;
    }

    @RequestMapping(value = "/createAdmin")
    public String createAdmin(@RequestParam("groupId") int groupId, @RequestParam("accountId") int accountId) {
        accountService.createAdminGroupByAccountId(accountId, groupId);
        return "redirect:/members?id=" + groupId;
    }

    @RequestMapping(value = "/groupAccept", method = RequestMethod.GET)
    public String groupAccept(@RequestParam("groupId") int groupId, @RequestParam("id") int accountId, @RequestParam("status") int status) {
        accountService.acceptGroupRequest(status, accountId, groupId);
        return "redirect:/members?id=" + groupId;
    }

    @RequestMapping(value = "/groups")
    public ModelAndView groupList(@SessionAttribute("account") Account account) {
        int id = account.getId();
        ModelAndView modelAndView = new ModelAndView("groupList");
        List<Group> groupsList = accountService.groupList(id);
        modelAndView.addObject("groupList", groupsList);
        return modelAndView;
    }

    @RequestMapping(value = "/createNewGroup")
    public String createNewGroup() {
        return "createNewGroup";
    }
}