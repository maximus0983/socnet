package com.getjavajob.mandzhievm.web.controllers;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.JMSMassageFriendRequest;
import com.getjavajob.mandzhievm.service.AccountService;
import com.getjavajob.mandzhievm.service.JMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class FriendController {
    @Autowired
    AccountService accountService;
    @Autowired
    JMSService jmsService;

    @RequestMapping(value = "/friendReq", method = RequestMethod.GET)
    public String friendRequest(@RequestParam("id") int friendId, @SessionAttribute("account") Account actionUser) {
        accountService.friendRequest(actionUser.getId(), friendId, actionUser.getId());
        Account accountTo = accountService.getById(friendId);
        JMSMassageFriendRequest jms = new JMSMassageFriendRequest(actionUser.getEmail(), accountTo.getEmail(),
                actionUser.getFirstName(), actionUser.getLastName());
        jmsService.sendMessage(jms);
        return "redirect:/account?id=" + friendId;
    }

    @RequestMapping(value = "/FriendAccept", method = RequestMethod.GET)
    public String friendAccept(@RequestParam("id") int id, @RequestParam("status") int status,
                               @RequestParam("currentPage") String page, @SessionAttribute("account") Account account) {
        int currentUser = account.getId();
        accountService.acceptFriendRequest(status, currentUser, currentUser, id);
        if (page.equals("friends")) {
            return "redirect:/friendsList";
        } else {
            return "redirect:/account?id=" + id;
        }
    }

    @RequestMapping(value = "/friendsList", method = RequestMethod.GET)
    public ModelAndView friendList(@SessionAttribute("account") Account account) {
        ModelAndView modelAndView = new ModelAndView("friends");
        int id = account.getId();
        List<Account> friendsList = accountService.getAllFriends(id);
        modelAndView.addObject("friends", friendsList);
        return modelAndView;
    }
}
