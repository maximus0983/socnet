package com.getjavajob.mandzhievm.web.interceptors;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationSuccessHandler.class);
    @Autowired
    AccountService accountService;

    private boolean isRememberMeAuthenticated() {

        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        logger.debug(authentication.toString());
        return RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException {
        HttpSession session = httpServletRequest.getSession();
        String uri = httpServletRequest.getRequestURI();
        Account account = accountService.findEmail(authentication.getName());
        session.setAttribute("account", account);
        List<Account> pendingAccounts = accountService.pendingRequest(account.getId());
        session.setAttribute("pendingFriendsList", pendingAccounts);
        if (isRememberMeAuthenticated()) {
            logger.debug("remember true");
            if (uri.endsWith("/")) {
                logger.debug("uri /");
                httpServletResponse
                        .sendRedirect(httpServletRequest.getContextPath() + "/account?id=" + account.getId());
            } else {
                if (uri.endsWith("/account")) {
                    logger.debug("uri acount");
                    httpServletResponse
                            .sendRedirect(httpServletRequest.getContextPath() + "/account?id=" + account.getId());
                } else {
                    httpServletResponse.sendRedirect(uri);
                }
            }
        } else {
            logger.debug("authentication success");
            httpServletResponse
                    .sendRedirect(httpServletRequest.getContextPath() + "/account?id=" + account.getId());
        }
    }
}
