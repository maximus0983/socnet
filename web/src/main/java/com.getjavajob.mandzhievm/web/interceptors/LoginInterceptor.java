package com.getjavajob.mandzhievm.web.interceptors;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);//todo исправить

    @Autowired
    private AccountService accountService;// todo область видимости

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler) throws Exception {
        List<Account> pendingAccounts;//todo hot key
        resp.setContentType("text/html");
        HttpSession session = req.getSession(false);
        String uri = req.getRequestURI();
        boolean result = false;
        if (session == null || session.getAttribute("account") == null) {
            Cookie[] cookies = req.getCookies();
            String email = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if ("email".equals(cookie.getName())) {
                        email = cookie.getValue();
                    }
                }
            }
            if (!email.isEmpty()) {
                session = req.getSession();
                Account account = accountService.findEmail(email);
                if (account != null) {
                    session.setAttribute("account", account);
                    pendingAccounts = accountService.pendingRequest(account.getId());
                    session.setAttribute("pendingFriendsList", pendingAccounts);
                    if (uri.endsWith("/") || uri.endsWith("/login") || uri.endsWith("/logout") ||
                            uri.endsWith("/registration") || uri.endsWith("/registr")) {
                        resp.sendRedirect(req.getContextPath() + "/account?id=" + account.getId());
                        result = false;
                    } else {
                        result = true;
                    }
                }
            } else if (uri.endsWith("/") || uri.endsWith("/login") || uri.endsWith("/logout") ||
                    uri.endsWith("/registration") || uri.endsWith("/registr")) {
                result = true;
            } else {
                resp.sendRedirect(req.getContextPath() + "/");
                result = false;
            }
        } else {
            logger.debug("2");
            Account account = (Account) session.getAttribute("account");
            logger.debug("id" + account.getId());
            pendingAccounts = accountService.pendingRequest(account.getId());
            session.setAttribute("pendingFriendsList", pendingAccounts);
            if (uri.endsWith("/") || uri.endsWith("/login") || uri.endsWith("/logout") ||
                    uri.endsWith("/registration") || uri.endsWith("/registr")) {
                resp.sendRedirect(req.getContextPath() + "/account?id=" + account.getId());
                logger.debug("3");
                result = false;
            } else {
                logger.debug("4");
                result = true;
            }
        }
        return result;
    }
}