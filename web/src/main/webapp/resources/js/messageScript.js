$(function () {
    connect();

    function connect() {
        var context = $('#context').val();
        var socket = new SockJS(context + '/greeting');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, function (frame) {
            stompClient.subscribe("/user/queue/errors", function (message) {
                    showMessageOutput(JSON.parse(message.body));
                }
            );
            stompClient.subscribe("/user/queue/reply", function (message) {
                showMessageOutput(JSON.parse(message.body));
            });
        }, function (error) {
            alert("STOMP error " + error);
        });
    }

    function disconnect() {
        if (stompClient != null) {
            stompClient.close();
        }
        setConnected(false);
        console.log("Disconnected");
    }

    $("#sendMessage").click(function () {
        var from = document.getElementById('from').value;
        var text = document.getElementById('text').value;
        $('#text').val('');
        var to = document.getElementById('to').value;
        var id = document.getElementById('id').value;
        stompClient.send("/app/greeting", {},
            JSON.stringify({'from': from, 'text': text, 'to': to, 'id': id}));
    })

    function showMessageOutput(message) {
        var html = "<br/>"
        var photo = $('#photo').val();
        var name = "";
        if (message.from == $('#from').val())
            name = $('#name').val();
        else name = $('#toname').val();
        if (photo !== null)
            photo = "<img src=image?id=" + message.id + " width=30 height=30>"
        else
            photo = "<img src=resources/images/default.jpg width=30 height=30>"
        html += photo + " " + name + " : " + message.text + "<br/>";
        $("#response").prepend(html);
    }
})