$(function () {
    $("#ajaxSearch").autocomplete({
        delay: 0,
        source: function (request, response) {
            $.ajax({
                url: "quickSearch",
                data: {
                    search: request.term
                },
                success: function (data) {
                    response($.map(data, function (obj, i) {
                        if (Object.keys(obj)[0] == "id") {
                            return {
                                value: window.location.pathname.split('/')[0] + "account?id=" + obj.id,
                                label: "account: " + obj.firstName + ' ' + obj.lastName
                            };
                        }
                        else {
                            return {
                                value: window.location.pathname.split('/')[0] + "group?id=" + obj.idGroup,
                                label: "group: " + obj.nameGroup
                            };
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            window.location.href = ui.item.value;
        }
    });
});