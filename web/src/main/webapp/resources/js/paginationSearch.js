$(function () {

    var page = 1,
        pageGroup = 1,
        pageLimit = 4;
    totalAccount = $('#totalAccount').val();
    totalGroup = $('#totalGroup').val();
    search = $('#searchAjax').val();

    fetchData();
    fetchGroupData();

    console.log("totalAccount", totalAccount);
    console.log("totalGroup", totalGroup)

    $("#accountPrev").click(function () {
        if (page > 1) {
            page--;
            fetchData();
        }
        console.log(page);
    });

    $("#accountNext").click(function () {
        if (page * pageLimit < totalAccount) {
            page++;
            fetchData();
            console.log(page);
        }
        console.log(page);
    });

    $("#groupPrev").click(function () {
        if (pageGroup > 1) {
            pageGroup--;
            fetchGroupData();
        }
        console.log(pageGroup);
    });

    $("#groupNext").click(function () {
        if (pageGroup * pageLimit < totalGroup) {
            pageGroup++;
            fetchGroupData();
            console.log(pageGroup);
        }
        console.log(pageGroup);
    });

    function fetchData() {
        $.ajax({
            url: "paginationAjaxSearch",
            type: "GET",

            data: {
                search: search,
                page: page,
                pageLimit: pageLimit
            },
            success: function (data) {
                var dataArr = data;
                var html = "<br>";
                for (var i = 0; i < dataArr.length; i++) {
                    if (Object.keys(dataArr[i])[0] == "id") {
                        var photo = "";
                        if (dataArr[i].photo !== null)
                            photo = "<img src=image?id=" + dataArr[i].id + " width=60 height=60>"
                        else
                            photo = "<img src=resources/images/default.jpg width=60 height=60>"

                        html += "<a href=account?id=" + dataArr[i].id + ">" +
                            photo + " " + dataArr[i].lastName + " " + dataArr[i].firstName +
                            "</a><br>"

                    }
                }
                $("#result").html(html);
            }
        });
    }

    function fetchGroupData() {
        $.ajax({
            url: "paginationAjaxSearch",
            type: "GET",
            data: {
                search: search,
                page: pageGroup,
                pageLimit: pageLimit
            },
            success: function (data) {
                var dataArr = data;
                var html = "<br>";
                for (var i = 0; i < dataArr.length; i++) {
                    if (Object.keys(dataArr[i])[0] !== "id") {
                        var photo = "";
                        if (dataArr[i].photo !== null)
                            photo = "<img src=imageGroup?id=" + dataArr[i].idGroup + " width=60 height=60>"
                        else
                            photo = "<img src=resources/images/group.png width=60 height=60>"

                        html += "<a href=group?id=" + dataArr[i].idGroup + ">" +
                            photo + " " + dataArr[i].nameGroup +
                            "</a><br>"
                    }
                }
                $("#groupsResult").html(html);
            }
        })
    }
})