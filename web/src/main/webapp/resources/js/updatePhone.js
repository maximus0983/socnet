jQuery(document).ready(function () {
    $('#addPhone').click(function (e) {
        e.preventDefault();
        $(".error").remove();
        var indexPhone = $('.phoneNumber').length;
        var number = $('#homePhone').val();
        var regex = /^((\+7|7|8)+([0-9]){10})$/;
        if (regex.test(number)) {
            $('#phone').append('<div class="form-group phoneNumber"><label for="homePhone">Home phone:</label>' +
                '<div class="form-inline"><input type="text" class="form-control col-sm-9" id="phones' + indexPhone +
                '" name="homePhone" value="' +
                number + '" ><button type="button" class="btn form-group col-sm-3 deletePhone">delete</button></div></div>');
            $('#homePhone').val('');
            indexPhone++;
        }
        else {
            $('#homePhone').before('<span class="error" style="color:red" >Некорректный номер</span>');
        }
    });

    $(document).on('click', '.deletePhone', (function (e) {
        $(this).parent().parent().remove();
    }));

    $(document).on('click', '.saveC', function (e) {
        e.preventDefault();
        $('#myModal').modal('show');
    });

    $('#homePhone').tooltip();

    $("#datepicker").datepicker({
        format: 'yyyy-mm-dd',
        locale: 'ru',
        autoclose: true
    });
})
