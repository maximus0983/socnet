<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Socnet</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body class="text-center">
<div class="container">
    <div class="row">
        <div class="col-sm-4  offset-md-4 align-self-center">

            <c:if test="${param.login_error}">
  <span style="color: red; ">
    Your login attempt was not successful, try again.<br/><br/>
    Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
  </span>
            </c:if>
            <form class="form-signin" action="<c:url value='/login' />" method="post">
                <span style="color: red;">${message}</span>
                <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                <label for="email" class="sr-only">Email address</label>
                <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required
                       autofocus>
                <label for="password" class="sr-only">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password"
                       required>
                <div class="checkbox mb-3">
                    <label>
                        <input id="remember-me" type="checkbox" name="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <label>
                    <h5><a href="registration">Registration</a></h5>
                </label>
                <%--<p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>--%>
            </form>
        </div>
    </div>
</div>

</body>
</html>