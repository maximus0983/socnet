<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Groups</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="conteiner text center>
		<div class=" row
">
<div class="col-sm-8 col-lg-6">

    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">Groups</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${groupList}" var="groups">
            <tr>
                <td>
                    <a href="<c:url value = 'group?id=${groups.idGroup}'/>">
                        <c:choose>
                            <c:when test="${groups.photo!=null}">
                                <img src="imageGroup?id=${groups.idGroup}" width="60" height="60">
                            </c:when>
                            <c:when test="${groups.photo==null}">
                                <img src="resources/images/group.png" width="60" height="60">
                            </c:when>
                        </c:choose> ${groups.nameGroup}
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</div>
</div>
</body>
</html>