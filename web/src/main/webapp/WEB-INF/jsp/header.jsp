<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        html {
            font-size: 13px;
        }

        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-info">
    <a class="navbar-brand" href="#"><h1>Socnet</h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link text-light" href="<c:url value = 'account?id=${sessionScope.account.id}'/>">
                    <h3>User: ${sessionScope.account.firstName}<span class="sr-only">(current)</span></h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light " href="<c:url value = 'friendsList'/>"><h3>Friends
                    <c:if test="${pendingFriendsList.size() != 0}">
        		 <span class="badge badge-light">
                         ${pendingFriendsList.size()}
                 </span></c:if>
                </h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light " href="<c:url value = 'groups'/>"><h3>Groups</h3></a>
            </li>
        </ul>

        <nav class="navbar navbar-dark bg-info">
            <form class="form-inline my-2 my-lg-0" action="search" method="get">
                <input id="ajaxSearch" class="form-control mr-sm-2" type="search" placeholder="Search" name="search"
                       aria-label="Search">
                <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
            </form>
        </nav>
        <form class="form-inline my-2 my-sm-0" action="<c:url value="/logout"/>" method="post">
            <button class="btn btn-outline-light my-2 my-sm-0 " type="submit">Log out</button>
        </form>
    </div>
</nav>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<spring:url value="resources/js/qSearch.js" var="qSearchJS"/>
<script src="${qSearchJS}"></script>
</body>
</html>