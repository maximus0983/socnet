<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Group requests</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="container text center>
		<div class=" row">
<div class="col-sm-8 col-lg-6">

    <c:if test="${user=='admin'}">
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th scope="col">Group requests</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${pendingList}" var="pendings">
                <tr>
                    <td>
                        <a href="<c:url value = 'account?id=${pendings.id}'/>">
                            <c:choose>
                                <c:when test="${pendings.photo!=null}">
                                    <img src="image?id=${pendings.id}" width="60" height="60">
                                </c:when>
                                <c:when test="${pendings.photo==null}">
                                    <img src="resources/images/default.jpg" width="60" height="60">
                                </c:when>
                            </c:choose> ${pendings.lastName} ${pendings.firstName}
                        </a>
                        <a href="<c:url value = 'groupAccept?id=${pendings.id}&status=1&groupId=${groupId}'/>">
                            <button type="submit" class="btn btn-info sm">Accept</button>
                        </a>
                        <a href="<c:url value = 'groupAccept?id=${pendings.id}&status=2&groupId=${groupId}'/>">
                            <button type="submit" class="btn btn-info sm">Reject</button>
                        </a>
                        <a href="<c:url value = 'groupAccept?id=${pendings.id}&status=3&groupId=${groupId}'/>">
                            <button type="submit" class="btn btn-info sm">Block</button>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">Group members</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${members}" var="accounts">
            <tr>
                <td>
                    <a href="<c:url value = 'account?id=${accounts.id}'/>">
                        <c:choose>
                            <c:when test="${accounts.photo!=null}">
                                <img src="image?id=${accounts.id}" width="60" height="60">
                            </c:when>
                            <c:when test="${accounts.photo==null}">
                                <img src="resources/images/default.jpg" width="60" height="60">
                            </c:when>
                        </c:choose> ${accounts.lastName} ${accounts.firstName}
                    </a>
                    <c:if test="${user=='admin'}">
                        <a href="<c:url value = 'deleteFromGroup?accountId=${accounts.id}&groupId=${groupId}'/>">
                            <button type="submit" class="btn btn-info sm">Delete</button>
                        </a>
                        <a href="<c:url value = 'createAdmin?accountId=${accounts.id}&groupId=${groupId}'/>">
                            <button type="submit" class="btn btn-info sm">Create Admin</button>
                        </a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</div>
</div>
</body>
</html>