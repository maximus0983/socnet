<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Chat WebSocket</title>
    <script src="webjars/jquery/3.3.1/jquery.js"></script>
    <script src="webjars/sockjs-client/1.1.2/sockjs.js"></script>
    <script src="webjars/stomp-websocket/2.3.3-1/stomp.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-6 align-self-start">
            <div>
                <div>
                    <input type="hidden" id="from" value="${account.email}"/>
                    <input type="hidden" id="photo" value="${account.photo}"/>
                    <input type="hidden" id="id" value="${account.id}"/>
                    <input type="hidden" id="to" value="${to}"/>
                    <input type="hidden" id="toname" value="${toName}"/>
                    <input type="hidden" id="name" value="${account.firstName}"/>
                    <input type="hidden" id="context" value="${pageContext.request.contextPath}"/>
                </div>
                <br/>
                <br/>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="text" placeholder="Write a message...">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="button" id="sendMessage">Send</button>
                    </div>
                </div>
                <p id="response"></p>
                <div class="form-group">
                    <c:forEach items="${messages}" var="mess" varStatus="i">
                        <div class="form-group">
                                <c:choose>
                                    <c:when test="${mess.from.photo != null}">
                                        <img src="image?id=${mess.from.id}" width="30" height="30">
                                    </c:when>
                                    <c:when test="${mess.from.photo == null}">
                                        <img src="${context}/images/default.jpg" width="30" height="30">
                                    </c:when>
                                </c:choose>
                                <c:out value="${mess.from.firstName} : ${mess.text}"/>
                        </div>
                    </c:forEach>
                </div>

            </div>
        </div>
        <div class="col-6 align-self-start">
        </div>
    </div>
</div>
<script src="resources/js/messageScript.js"></script>
</body>
</html>