<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-4  offset-md-4 align-self-center">
            <form action="<c:url value='/registr' />" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Email address</label>
                    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" required>
                </div>
                <div class="form-group">
                    <label for="firstName">First name:</label>
                    <input type="text" class="form-control" name="firstName" id="firstName" required>
                </div>
                <div class="form-group">
                    <label for="lastName">Last name:</label>
                    <input type="text" class="form-control" name="lastName" id="lastName" required>
                </div>
                <div class="form-group">
                    <label for="middleName">Middle name:</label>
                    <input type="text" class="form-control" name="middleName" id="middleName" required>
                </div>
                <div class="form-group">
                    <label for="accountBirthDay">Birth day:</label>
                    <input type="text" class="form-control" id="datepicker" name="accountBirthDay"
                           value="${account.birthDay}">
                </div>
                <div class="form-group">
                    <label for="skype">Skype:</label>
                    <input type="text" class="form-control" name="skype" id="skype">
                </div>
                <div class="form-group">
                    <label for="icq">Icq:</label>
                    <input type="text" class="form-control" name="icq" id="icq">
                </div>
                <div class="form-group" id="phone">
                    <c:forEach items="${myPhones}" var="phones" varStatus="i">
                        <div class="form-group phoneNumber">
                            <label for="homePhone">Home phone:</label>
                            <div class="form-inline">
                                <input type="text" class="form-control col-sm-9" id="phones${i.index}" name="homePhone"
                                       value="<c:out value="${phones.phone}"/>">
                                <button type="button" class="btn form-group col-sm-3 deletePhone">delete</button>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class="form-group">
                    <label for="hP">Home phone:</label>
                    <input type="text" class="form-control" data-toggle="tooltip" data-placement="right" id="homePhone"
                           title="Введите номер в формате +71234567890" name="hP">
                </div>
                <button type="button" class="btn btn-primary" id="addPhone">добавить</button>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <div class="form-group">
                    <label for="file">Photo</label>
                    <input type="file" class="form-control-file" name="file" id="photo">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <spring:url value="resources/js/updatePhone.js" var="updatePhoneJS"/>
    <script src="${updatePhoneJS}"></script>
</div>
</body>
</html>