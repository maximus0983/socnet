<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Search</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
<jsp:include page="header.jsp"/>

<div class="container text center>
	<div class=" row
">
<div class="form-group">
    <input type="hidden" class="form-control-file" name="searchAjax" id="searchAjax" value="${search}">
    <input type="hidden" class="form-control-file" name="totalAccount" id="totalAccount" value="${totalAccount}">
    <input type="hidden" class="form-control-file" name="totalGroup" id="totalGroup" value="${totalGroup}">
</div>
<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Accounts</a></li>
        <li><a href="#tabs-2">Groups</a></li>
    </ul>
    <div id="tabs-1">
        <div class="nav-btn-container">
            <button class="btn btn-info sm" id="accountPrev">prev</button>
            <button class="btn btn-info sm" id="accountNext">next</button>
        </div>
        <div id="result"></div>
    </div>

    <div id="tabs-2">
        <div class="nav-btn-container">
            <button class="btn btn-info sm" id="groupPrev">prev</button>
            <button class="btn btn-info sm" id="groupNext">next</button>
        </div>
        <div id="groupsResult"></div>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<spring:url value="resources/js/paginationSearch.js" var="paginationSearchJS"/>
<script src="${paginationSearchJS}"></script>
<script>
    $(function () {
        $("#tabs").tabs();
    });
</script>
</body>
</html>