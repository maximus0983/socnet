<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Friends</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="conteiner text center>
		<div class=" row
">
<div class="col-sm-8 col-lg-6">
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">Friend requests</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pendingFriendsList}" var="pendings">
            <tr>
                <td>
                    <a href="<c:url value = 'account?id=${pendings.id}'/>">
                        <c:choose>
                            <c:when test="${pendings.photo!=null}">
                                <img src="image?id=${pendings.id}" width="60" height="60">
                            </c:when>
                            <c:when test="${pendings.photo==null}">
                                <img src="resources/images/default.jpg" width="60" height="60">
                            </c:when>
                        </c:choose> ${pendings.lastName} ${pendings.firstName}
                    </a>
                    <a href="<c:url value = 'FriendAccept?id=${pendings.id}&status=1&currentPage=friends'/>">
                        <button type="submit" class="btn btn-info sm">Accept</button>
                    </a>
                    <a href="<c:url value = 'FriendAccept?id=${pendings.id}&status=2&currentPage=friends'/>">
                        <button type="submit" class="btn btn-info sm">Reject</button>
                    </a>
                    <a href="<c:url value = 'FriendAccept?id=${pendings.id}&status=3&currentPage=friends'/>">
                        <button type="submit" class="btn btn-info sm">Block</button>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">Friends</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${friends}" var="accounts">
            <tr>
                <td>
                    <a href="<c:url value = 'account?id=${accounts.id}'/>">
                        <c:choose>
                            <c:when test="${accounts.photo!=null}">
                                <img src="image?id=${accounts.id}" width="60" height="60">
                            </c:when>
                            <c:when test="${accounts.photo==null}">
                                <img src="resources/images/default.jpg" width="60" height="60">
                            </c:when>
                        </c:choose> ${accounts.lastName} ${accounts.firstName}
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</div>
</div>
</body>
</html>