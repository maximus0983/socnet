<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>create group</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-sm-4  offset-md-4 align-self-center">
            <form action="updateGroup" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" class="form-control" name="idGroup" id="idGroup" value="${group.idGroup}">
                </div>
                <div class="form-group">
                    <label for="nameGroup">Group name</label>
                    <input type="text" class="form-control" name="nameGroup" id="nameGroup" value="${group.nameGroup}">
                </div>
                <div class="form-group">
                    <label for="info">Info:</label>
                    <input type="text" class="form-control" name="info" id="info" value="${group.info}">
                </div>
                <div class="form-group">
                    <label for="file">Photo</label>
                    <input type="file" class="form-control-file" name="file" id="photo">
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control-file" name="idgroup" id="idgroup" value="${group.idGroup}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <a href="<c:url value = 'group?id=${group.idGroup}'/>">
                <button type="submit" class="btn btn-primary">Cancel</button>
            </a>

        </div>
    </div>
</div>
</body>
</html>