<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Update Account</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

</head>
<body>
<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-4  offset-md-4 align-self-center">
            <form action="updateAccount" method="post" enctype="multipart/form-data" id="myForm">
                <div class="form-group">
                    <input type="hidden" class="form-control-file" name="id" id="id" value="${account.id}">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" value="${account.email}">
                </div>
                <div class="form-group">
                    <label for="firstName">First name:</label>
                    <input type="text" class="form-control" id="firstName" name="firstName"
                           value="${account.firstName}">
                </div>
                <div class="form-group">
                    <label for="lastName">Last name:</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" value="${account.lastName}">
                </div>
                <div class="form-group">
                    <label for="middleName">Middle name:</label>
                    <input type="text" class="form-control" id="middleName" name="middleName"
                           value="${account.middleName}">
                </div>
                <div class="form-group">
                    <label for="accountBirthDay">Birth day:</label>
                    <input type="text" class="form-control" id="datepicker" name="accountBirthDay"
                           value="${account.birthDay}">
                </div>
                <div class="form-group">
                    <label for="skype">Skype:</label>
                    <input type="text" class="form-control" id="skype" name="skype" value="${account.skype}">
                </div>
                <div class="form-group">
                    <label for="icq">Icq:</label>
                    <input type="text" class="form-control" id="icq" name="icq" value="${account.icq}">
                </div>
                <div class="form-group" id="phone">
                    <c:forEach items="${account.phones}" var="phones" varStatus="i">
                        <div class="form-group phoneNumber">
                            <label for="homePhone">Home phone:</label>
                            <div class="form-inline">
                                <input type="text" class="form-control col-sm-9" id="phones${i.index}" name="homePhone"
                                       value="<c:out value="${phones.phone}"/>">
                                <button type="button" class="btn form-group col-sm-3 deletePhone">delete</button>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class="form-group">
                    <label for="hP">Home phone:</label>
                    <input type="text" class="form-control" data-toggle="tooltip" data-placement="right" id="homePhone"
                           title="Введите номер в формате +71234567890" name="hP">

                </div>
                <button type="button" class="btn btn-primary" id="addPhone">добавить</button>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password"
                           value="${account.password}">
                </div>
                <div class="form-group">
                    <label for="file">Photo</label>
                    <input type="file" class="form-control-file" id="photo" name="file">
                </div>
                <button type="button" class="btn btn-primary saveC" data-toggle="modal" data-target="#myModal">Submit
                </button>

                <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>

                            </div>
                            <div class="modal-body">
                                <h4>Уверены, что хотите сохранить изменения?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                                <button type="submit" class="btn btn-primary">Да</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal --></form>

            <div class="btn-group-vertical">
                <a href="writeAccount">
                    <button type="submit" class="btn btn-link sm">Записать в XML</button>
                </a>
                <form action="readFile" method="post" enctype="multipart/form-data">
                    <input type="file" class="form-control-file" name="file">
                    <button type="submit" class="btn btn-link sm">Прочитать из XML</button>
                </form>
                <%--<button type="submit" class="btn btn-link sm">Прочитать из XML</button>--%>
            </div>
        </div>
    </div>
</div>

<%--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--%>
<script src="webjars/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<spring:url value="resources/js/updatePhone.js" var="updatePhoneJS"/>
<script src="${updatePhoneJS}"></script>
</body>
</html>