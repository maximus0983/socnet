<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Account</title>
    <%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>--%>
    <link rel="stylesheet" href="webjars/bootstrap/4.3.1/dist/css/bootstrap.min.css"/>
</head>
<style>
    html {
        font-size: 13px;
    }
</style>
<body>
<jsp:include page="header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-md-3 well">
            <div class="well">
                <c:set var="context" value="${pageContext.request.contextPath}"/>
                <c:choose>
                    <c:when test="${account.photo != null}">
                        <img src="image?id=${account.id}" width="200" height="200">
                    </c:when>
                    <c:when test="${account.photo == null}">
                        <img src="/resources/images/default.jpg" width="200" height="200">
                    </c:when>
                </c:choose>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    First name:
                    ${account.firstName}</li>
                <li class="list-group-item">
                    Last name:
                    ${account.lastName}</li>
                <li class="list-group-item">
                    Middle name:
                    ${account.middleName}</li>
                <li class="list-group-item">
                    Birth day:
                    ${account.birthDay}</li>
                <li class="list-group-item">
                    Skype:
                    <c:if test="${account.skype != null}">
                        <c:out value="${account.skype}"/>
                    </c:if></li>
                <li class="list-group-item">
                    Icq:
                    <c:if test="${account.icq != null}">
                        <c:out value="${account.icq}"/>
                    </c:if></li>
                <c:forEach items="${account.phones}" var="phones">
                    <li class="list-group-item">
                        Home phone:
                        <c:out value="${phones.phone}"/>
                    </li>
                </c:forEach>
            </ul>
            <c:choose>
                <c:when test="${account.id==sessionScope.account.id}">
                    <a href="update">
                        <button type="submit" class="btn btn-info sm" value="update">update</button>
                    </a>
                </c:when>
                <c:when test="${status == -1}">
                    <a href="<c:url value = 'friendReq?id=${account.id}'/>">
                        <button type="submit" class="btn btn-info sm">Add as Friend</button>
                    </a>
                </c:when>
                <c:when test="${status == 2}">
                    <a href="<c:url value = 'FriendAccept?id=${account.id}&status=1&currentPage=account'/>">
                        <button type="submit" class="btn btn-info sm">Add as Friend</button>
                    </a>
                </c:when>
                <c:when test="${status == 0}">
                    <button type="submit" class="btn btn-secondary sm disabled">Request has been sent</button>
                    <a href="<c:url value = 'FriendAccept?id=${account.id}&status=2&currentPage=account'/>">
                        <button type="submit" class="btn btn-info sm">Cancel request</button>
                    </a>
                </c:when>
                <c:when test="${status == 1}">
                    <a href="<c:url value = 'FriendAccept?id=${account.id}&status=2&currentPage=account'/>">
                        <button type="submit" class="btn btn-info sm">Unfriend</button>
                    </a>
                </c:when>
                <c:when test="${status == 3}">
                    <a href="<c:url value = 'FriendAccept?id=${account.id}&status=1&currentPage=account'/>">
                        <button type="submit" class="btn btn-info sm">Unblock</button>
                    </a>
                </c:when>
            </c:choose>

            <sec:authorize access="hasRole('ADMIN')">
                <a href="<c:url value = 'deleteAccount?id=${account.id}'/>">
                    <button type="submit" class="btn btn-info sm">Delete account</button>
                </a>
            </sec:authorize>

        </div>

        <div class="col-md-6 well">
            <div class="align-self-center">
            <h2>Messages</h2>
            </div>
            <div class="aaa align-self-start">
            <br/>
            <br/>
            <br/>
            <form action="wallMessage" method="post">
                    <input type="hidden" name="id" value="${account.id}"/>
                <div class="form-group">
                    <label for="Input1">написать сообщение на стене</label>
                    <textarea class="form-control" name="wallMessage" id="Input1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-info align-self-start">отправить</button>
            </form>
                <ul class="list-group">
                    <c:forEach items="${messages}" var="mess" varStatus="i">
                      <li class="list-group-item">
                          <a href="<c:url value = 'account?id=${mess.from.id}'/>">
                          <c:choose>
                            <c:when test="${mess.from.photo != null}">
                                <img src="image?id=${mess.from.id}" width="30" height="30">
                            </c:when>
                            <c:when test="${mess.from.photo == null}">
                                <img src="${context}/images/default.jpg" width="30" height="30">
                            </c:when>
                        </c:choose>
                                  ${mess.from.firstName}</a>
                            <p>${mess.text}</p>
                      </li>
                    </c:forEach>
                </ul>
            </div>
            </div>

        <div class="col-md-3 well"><br>
            <p>
                <c:if test="${account.id!=sessionScope.account.id}">
                    <a href="<c:url value = '/message?id=${account.id}'/>">
                        <button type="submit" class="btn btn-info sm">Написать сообщение</button>
                    </a>
                </c:if>
            </p>
            <c:if test="${account.id==sessionScope.account.id}">
            <a href="<c:url value = '/createNewGroup'/>">
                <button type="submit" class="btn btn-info sm">Create group</button>
            </a>
            </c:if>
            <ul class="list-group list-group-flush">
                <c:forEach items="${requestScope.groups}" var="groups">
                    <li class="list-group-item">
                        <a href="<c:url value = 'group?id=${groups.id}'/>">
                                ${groups.nameGroup}<br>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>
</body>
</html>