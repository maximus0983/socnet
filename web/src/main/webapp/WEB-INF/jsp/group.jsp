<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Group</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container text-center">
    <div class="row">
        <div class="col-sm-3">
            <div class="well">
                <c:set var="context" value="${pageContext.request.contextPath}"/>
                <c:choose>
                    <c:when test="${group.photo!=null}">
                        <img src="imageGroup?id=${group.idGroup}" width="300" height="300">
                    </c:when>
                    <c:when test="${group.photo==null}">
                        <img src="resources/images/group.png" width="300" height="300">
                    </c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${user=='admin'}">
                        <a href="<c:url value = 'updateGroupPage?id=${group.idGroup}'/>">
                            <button type="submit" class="btn btn-info sm">Update Group
                            </button>
                        </a>
                        <a href="<c:url value = 'deleteGroup?id=${group.idGroup}'/>">
                            <button type="submit" class="btn btn-info sm">Delete group</button>
                        </a>
                    </c:when>
                    <c:when test="${status == -1}">
                        <a href="<c:url value = 'groupRequest?id=${group.idGroup}'/>">
                            <button type="submit" class="btn btn-info sm">request to group</button>
                        </a>
                    </c:when>
                    <c:when test="${status == 0}">
                        <button type="submit" class="btn btn-secondary sm disabled">Request has been sent</button>
                        <a href="<c:url value = 'deleteFromGroup?groupId=${group.idGroup}&accountId=${account.id}'/>">
                            <button type="submit" class="btn btn-info sm">Cancel request</button>
                        </a>
                    </c:when>
                    <c:when test="${status == 1}">
                        <a href="<c:url value = 'deleteFromGroup?groupId=${group.idGroup}&accountId=${account.id}'/>">
                            <button type="submit" class="btn btn-info sm">Out of group</button>
                        </a>
                    </c:when>
                </c:choose>
                <a href="<c:url value = 'members?id=${group.idGroup}'/>">
                    <button type="submit" class="btn btn-info sm">Group members
                        <c:if test="${pendingList.size() != 0 && user=='admin'}">
                                                   <span class="badge badge-light">
                                                           ${pendingList.size()}
                                                   </span>
                        </c:if>
                    </button>
                </a>
            </div>
        </div>

        <div class="col-sm-6">

            <h2>${group.nameGroup}</h2>
            <h3>Info: ${group.info}</h3>
            <h2>Messages</h2>
            <c:choose>
                <c:when test="${status==1}">
                    messages
                </c:when>
                <c:when test="${status!=1}">
                    Only group members can read messages
                </c:when>
            </c:choose>
        </div>

        <div class="col-sm-3">
            <h4>Administrators</h4>
            <ul class="list-group list-group-flush">
                <c:forEach items="${requestScope.admins}" var="accounts">
                    <li class="list-group-item">
                        <a href="<c:url value = 'account?id=${accounts.id}'/>">
                                ${accounts.lastName} ${accounts.firstName}
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>

    </div>
</div>
</body>
</html>