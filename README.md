### Socnetmandzhiev

**Functionality**

+ registration
+ authentication
+ display profile
+ edit profile
+ upload avatar
+ ajax search with pagination
+ ajax search with autocomplete
+ users export to xml
+ users import from xml
+ friends, groups
+ messages with WebSocket
+ sending to email events

**Tools:**  
   JDK 7,8, Spring 5, JPA 2/Hibernate 5, jQuery, Twitter Bootstrap 4, JavaScript, JUnit 4, JSP/JSTL, Mockito, Maven 3, Log4j, Spring-boot 2, Git/Bitbucket, Tomcat 9, MySQL, IntelliJIDEA, JMS/ActiveMQ
   
**Screenshots**
![Login page](screenshots/login.png)
![Edit page](screenshots/edit_account.png)
![Autocomplete](screenshots/autocomplete.png)
![Pagination](screenshots/pagination.png)
![Group](screenshots/group.png)

project hosted: https://socnetmandzhiev.herokuapp.com  
   login: admin@mail.ru  
   password: admin    
_  
**Mandzhiev Maxim**  
Training getJavaJob  
http://www.getjavajob.com    