package com.getjavajob.mandzhievm.service;

import com.getjavajob.mandzhievm.common.JMSMassageFriendRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JMSService {
    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(JMSMassageFriendRequest jmsAccount){
        jmsTemplate.convertAndSend("friendRequestQueue",jmsAccount);
    }
}
