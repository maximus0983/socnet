package com.getjavajob.mandzhievm.service;

import com.getjavajob.mandzhievm.common.*;
import com.getjavajob.mandzhievm.common.repository.MessageRepository;
import com.getjavajob.mandzhievm.dao.AccountDao;
import com.getjavajob.mandzhievm.dao.AccountGroupDao;
import com.getjavajob.mandzhievm.dao.FriendDao;
import com.getjavajob.mandzhievm.dao.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountDao dao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private FriendDao friendDao;
    @Autowired
    private AccountGroupDao accountGroupDao;
    @Autowired
    private MessageRepository messageRepository;

    public void setDao(AccountDao dao) {
        this.dao = dao;
    }

    public void setDao(GroupDao dao) {
        this.groupDao = dao;
    }

    public void setDao(AccountGroupDao accountGroupDaoDao) {
        this.accountGroupDao = accountGroupDaoDao;
    }

    @Transactional
    public List<Message> getMessages(Account from, Account to, String type) {
        return messageRepository.findMessage(from, to,type);
    }

    @Transactional
    public List<Message> getWallMessages(Account to,String type) {
        return messageRepository.findAllByToAndType(to,type);
    }

    @Transactional
    public void save(Message message) {
        messageRepository.save(message);
    }

    @Transactional(readOnly = true)
    public List<Account> getAllFriends(int accId) {
        List<Integer> accountId = friendDao.getAll(accId);
        List<Account> accounts = new ArrayList<>();
        for (Integer anAccountId : accountId) {
            accounts.add(dao.findById(anAccountId));
        }
        return accounts;
    }

    @Transactional
    public void friendRequest(int accOne, int accTwo, int action) {
        friendDao.friendRequest(accOne, accTwo, action);
    }

    @Transactional
    public void groupRequest(int accId, int groupId, User user, int status) {
        accountGroupDao.groupRequest(accId, groupId, user, status);
    }

    @Transactional
    public void acceptFriendRequest(int status, int action, int accOne, int accTwo) {
        friendDao.acceptFriendRequest(status, action, accOne, accTwo);
    }

    @Transactional
    public void acceptGroupRequest(int status, int idaccount, int groupId) {
        accountGroupDao.acceptGroupRequest(status, idaccount, groupId);
    }

    @Transactional
    public List<Account> pendingRequest(int acc) {
        List<Integer> accountId = friendDao.pendingRequest(acc);
        List<Account> accounts = new ArrayList<>();
        for (Integer anAccountId : accountId) {
            accounts.add(dao.findById(anAccountId));
        }
        return accounts;
    }

    @Transactional
    public int friendRequestSent(int accOne, int accTwo) {
        return friendDao.friendRequestSent(accOne, accTwo);
    }

    @Transactional
    public int groupRequestSent(int groupId, int accountId) {
        return accountGroupDao.groupRequestSent(groupId, accountId);
    }

    @Transactional(readOnly = true)
    public String getUserById(int groupId, int accountId) {
        return accountGroupDao.getUser(groupId, accountId);
    }

    @Transactional(readOnly = true)
    public List<Object> paginationSearch(String search, int page, int pageLimit) {
        List<Object> result = new ArrayList<>();
        result.addAll(dao.doSearch(search, page, pageLimit));
        result.addAll(groupDao.doSearch(search, page, pageLimit));
        return result;
    }

    @Transactional(readOnly = true)
    public Long getCountFromAccountSearch(String search) {
        return dao.countFromAccountSearch(search);
    }

    @Transactional(readOnly = true)
    public Long getCountFromGroupSearch(String search) {
        return groupDao.countFromGroupSearch(search);
    }

    @Transactional
    public Account create(Account account, String[] newPhones) {
        updatePhones(account, newPhones);
        return dao.save(account);
    }

    @Transactional(readOnly = true)
    public Account getById(int id) {
        return dao.findById(id);
    }

    @Transactional(readOnly = true)
    public Group getGroupById(int id) {
        return groupDao.getById(id);
    }

    public void updatePhones(Account account, String[] newPhones) {
        if (account.getPhones().size() > 0) {
            for (Phone phone : account.getPhones()) {
                account.removePhone(phone);
            }
        }
        if (newPhones != null) {
            for (String newPhone : newPhones) {
                Phone phone = new Phone();
                phone.setPhone(newPhone);
                account.addPhone(phone);
            }
        }
    }

    @Transactional
    public Account update(Account account, String[] newPhones) {
        updatePhones(account, newPhones);
        return dao.save(account);
    }

    @Transactional
    public void create(Group group) {
        groupDao.create(group);
        accountGroupDao.groupRequest(group.getAccountId(), group.getIdGroup(), User.admin, 1);
    }

    @Transactional(readOnly = true)
    public List<Account> groupMembers(int groupId, User user) {
        return accountGroupDao.getAll(groupId, user);
    }

    @Transactional(readOnly = true)
    public List<Group> groupList(int accountId) {
        List<Integer> groupsId = accountGroupDao.getGroupsByAccountId(accountId);
        List<Group> groups = new ArrayList<>();
        for (Integer anGroupsId : groupsId) {
            groups.add(groupDao.getById(anGroupsId));
        }
        return groups;
    }

    @Transactional(readOnly = true)
    public List<Account> pendingGroupRequest(int groupId) {
        return accountGroupDao.pendingGroupRequest(groupId);
    }

    @Transactional
    public void deleteFromGroupByAccountId(int accountId, int groupId) {
        accountGroupDao.delete(accountId, groupId);
    }

    @Transactional
    public void update(Group group) {
        groupDao.updateById(group, group.getIdGroup());
    }

    @Transactional
    public void deleteGroup(int groupId) {
        groupDao.deleteById(groupId);
    }

    public List<Account> getAllAccount() {
        return dao.getAll();
    }

    @Transactional
    public List<Group> getAllGroup() {
        return groupDao.getAll();
    }

    @Transactional(readOnly = true)
    public Account findEmail(String email) {
        return dao.findEmail(email);
    }

    @Transactional(readOnly = true)
    public int getGroupByName(String name) {
        return groupDao.getByName(name).getIdGroup();
    }

    @Transactional
    public void createAdminGroupByAccountId(int accountId, int groupId) {
        accountGroupDao.createAdmin(accountId, groupId);
    }

    @Transactional
    @Secured(value = {"ROLE_ADMIN"})
    public void deleteAccount(int id) {
        dao.deleteAccount(id);
    }
}