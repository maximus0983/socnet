package com.getjavajob.mandzhievm.service;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.Group;
import com.getjavajob.mandzhievm.dao.AccountDao;
import com.getjavajob.mandzhievm.dao.AccountGroupDao;
import com.getjavajob.mandzhievm.dao.GroupDao;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceTest {
    private static AccountDao dao = mock(AccountDao.class);
    private static GroupDao groupDao = mock(GroupDao.class);
    private static AccountGroupDao accountGroupDao = mock(AccountGroupDao.class);
    private static AccountService accountService = new AccountService();

    @BeforeClass
    public static void setUp() throws Exception {
        accountService.setDao(dao);
        accountService.setDao(groupDao);
        accountService.setDao(accountGroupDao);
    }

    @Test
    public void create() throws Exception {
        Account account = new Account();
        account.setFirstName("bruce");
        account.setLastName("willis");
        account.setMiddleName("petrovich");
        when(dao.save(account)).thenReturn(account);
        assertEquals(account, accountService.create(account, new String[]{}));
    }

    @Test
    public void searchAccount() {
        Account account = new Account();
        account.setLastName("bruce");
        account.setFirstName("willis");
        account.setMiddleName("petrovich");
        when(dao.doSearch("bruce")).thenReturn(Arrays.asList(account));
        assertEquals("bruce", accountService.searchAccount("bruce").get(0).getLastName());
    }

    @Test
    public void searchGroup() {
        when(groupDao.doSearch("group")).thenReturn(Arrays.asList(new Group("group")));
        assertEquals("group", accountService.searchGroup("group").get(0).getNameGroup());
    }

    @Test
    public void update() {
        Account account = new Account();
        account.setFirstName("bruce");
        account.setLastName("willis");
        account.setMiddleName("petrovich");
        account.setId(2);
        when(dao.save(account)).thenReturn(account);
        assertEquals(account, accountService.update(account, new String[]{}));
    }

    @Test
    public void delete() throws Exception {
        Account account = new Account();
        account.setFirstName("bruce");
        account.setLastName("willis");
        account.setMiddleName("petrovich");
        account.setId(2);
        when(dao.deleteById(account.getId())).thenReturn(true);
        assertEquals(true, accountService.delete(account));
    }

    @Test
    public void getById() {
        Account account = new Account();
        account.setLastName("bruce");
        account.setFirstName("willis");
        account.setMiddleName("petrovich");
        when(dao.findById(1)).thenReturn(account);
        assertEquals("bruce", accountService.getById(1).getLastName());
    }

    @Test
    public void createGroup() throws Exception {
        Group group = new Group("group");
        when(groupDao.create(group)).thenReturn(true);
        when(accountGroupDao.groupRequest(1, 1, User.admin, 1)).thenReturn(true);
        assertEquals(true, accountService.create(group));
    }

    @Test
    public void updateGroup() throws Exception {
        Group group = new Group("group");
        group.setIdGroup(1);
        when(groupDao.updateById(group, group.getIdGroup())).thenReturn(true);
        assertEquals(true, accountService.update(group));
    }

    @Test
    public void deleteGroup() throws Exception {
        Group group = new Group("group");
        group.setIdGroup(1);
        when(groupDao.deleteById(group.getIdGroup())).thenReturn(true);
        assertEquals(true, accountService.deleteGroup(group.getIdGroup()));
    }

    @Test
    public void getAllAccount() {
        Account account = new Account();
        account.setLastName("bruce");
        account.setFirstName("willis");
        account.setMiddleName("petrovich");
        when(dao.getAll()).thenReturn(Arrays.asList(account));
        assertEquals("bruce", accountService.getAllAccount().get(0).getLastName());
    }
}