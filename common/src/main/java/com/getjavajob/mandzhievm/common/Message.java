package com.getjavajob.mandzhievm.common;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMessage;
    @ManyToOne
    @JoinColumn(name = "fromAccount")
    private Account from;
    private String text;
    private String time;
    @ManyToOne
    @JoinColumn(name = "toAccount")
    private Account to;
    private String type;

    public Message() {
    }

    public Message(Account from, String text, String time, Account to, String type) {
        this.from = from;
        this.text = text;
        this.time = time;
        this.to = to;
        this.type = type;
    }

    public Integer getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Integer idMessage) {
        this.idMessage = idMessage;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(idMessage, message.idMessage) &&
                Objects.equals(from, message.from) &&
                Objects.equals(text, message.text) &&
                Objects.equals(time, message.time) &&
                Objects.equals(to, message.to) &&
                Objects.equals(type, message.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMessage, from, text, time, to, type);
    }
}