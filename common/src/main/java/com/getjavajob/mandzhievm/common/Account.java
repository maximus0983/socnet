package com.getjavajob.mandzhievm.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.*;

@XmlRootElement
@Entity
@Table
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthDay;
    private String homeAddress;
    private String workAddress;
    private String email;
    private String icq;
    private String skype;
    private String additionalInfo;
    private String password;
    private LocalDate regDate;
    private int role;
    private byte[] photo;
    @JsonIgnore
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AccountGroup> groups = new ArrayList<>();//todo разобраться
    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Phone> phones = new HashSet<>();

    public Account() {
    }

    public Account(String lastName, String firstName, String middleName, LocalDate birthDay, String homeAddress,
                   String workAddress, String email, String icq, String skype,
                   String additionalInfo, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.additionalInfo = additionalInfo;
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public byte[] getPhoto() {
        return photo;
    }

    @XmlTransient
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPassword() {
        return password;
    }

    @XmlTransient
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    @XmlAttribute
    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    @XmlElement
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    @XmlElement
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    @XmlElement
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    @XmlElement
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    @XmlElement
    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    @XmlElement
    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    @XmlElement
    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    @XmlElement
    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    @XmlElement
    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    @XmlElement
    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @XmlElement
    public void setAdditionalInfo(String other) {
        this.additionalInfo = other;
    }

    public List<AccountGroup> getGroups() {
        return groups;
    }

    @XmlTransient
    public void setGroups(List<AccountGroup> groups) {
        this.groups = groups;
    }

    public void addGroup(Group group, AccountGroup accountGroup) {
        groups.add(accountGroup);
        group.getAccounts().add(accountGroup);
    }

    public void removeGroup(Group group) {
        for (int i = 0; i < groups.size(); i++) {
            AccountGroup accountGroup = groups.get(i);
            if (accountGroup.getAccount().equals(this) && accountGroup.getGroup().equals(group)) {
                groups.remove(accountGroup);
                accountGroup.getGroup().getAccounts().remove(accountGroup);
                accountGroup.setAccount(null);
                accountGroup.setGroup(null);
            }
        }
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    @XmlElement
    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public void addPhone(Phone phone) {
        phone.setAccount(this);
        getPhones().add(phone);
    }

    public void removePhone(Phone phone) {
        getPhones().remove(phone);
    }

    @Override
    public String toString() {
        return id + " " + lastName + " " + firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(lastName, account.lastName) &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(middleName, account.middleName) &&
                Objects.equals(birthDay, account.birthDay) &&
                Objects.equals(homeAddress, account.homeAddress) &&
                Objects.equals(workAddress, account.workAddress) &&
                Objects.equals(email, account.email) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInfo, account.additionalInfo) &&
                Objects.equals(password, account.password) &&
                Objects.equals(regDate, account.regDate) &&
                Objects.equals(phones, account.phones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, phones);
    }
}