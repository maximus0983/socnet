package com.getjavajob.mandzhievm.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table
public class Friends implements Serializable {
    @Id
    @Column(name = "acc_one_id")
    private int accountOne;
    @Id
    @Column(name = "acc_two_id")
    private int accountTwo;
    private int status;
    private int action;

    public Friends() {
    }

    public Friends(int accountOne, int accountTwo) {
        this.accountOne = accountOne;
        this.accountTwo = accountTwo;
    }


    public int getAccountOne() {
        return accountOne;
    }

    public void setAccountOne(int accountOne) {
        this.accountOne = accountOne;
    }

    public int getAccountTwo() {
        return accountTwo;
    }

    public void setAccountTwo(int accountTwo) {
        this.accountTwo = accountTwo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friends friends = (Friends) o;
        return accountOne == friends.accountOne &&
                accountTwo == friends.accountTwo &&
                status == friends.status &&
                action == friends.action;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountOne, accountTwo, status, action);
    }

    //    public Account getAccountOne() {
//        return accountOne;
//    }
//
//    public void setAccountOne(Account accountOne) {
//        this.accountOne = accountOne;
//    }
//
//    public Account getAccountTwo() {
//        return accountTwo;
//    }
//
//    public void setAccountTwo(Account accountTwo) {
//        this.accountTwo = accountTwo;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public int getAction() {
//        return action;
//    }
//
//    public void setAction(int action) {
//        this.action = action;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Friends friends = (Friends) o;
//
//        if (status != friends.status) return false;
//        if (action != friends.action) return false;
//        if (!Objects.equals(id, friends.id)) return false;
//        if (!Objects.equals(accountOne, friends.accountOne)) return false;
//        return Objects.equals(accountTwo, friends.accountTwo);
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (accountOne != null ? accountOne.hashCode() : 0);
//        result = 31 * result + (accountTwo != null ? accountTwo.hashCode() : 0);
//        result = 31 * result + status;
//        result = 31 * result + action;
//        return result;
//    }
}
