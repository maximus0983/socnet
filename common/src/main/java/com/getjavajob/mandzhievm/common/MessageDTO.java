package com.getjavajob.mandzhievm.common;

public class MessageDTO {
    private String from;
    private String text;
    private String time;
    private String to;
    private String id;

    public MessageDTO(String from, String text, String time, String to, String id) {
        this.from = from;
        this.text = text;
        this.time = time;
        this.to = to;
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
