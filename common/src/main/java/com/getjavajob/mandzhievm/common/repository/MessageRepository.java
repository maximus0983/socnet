package com.getjavajob.mandzhievm.common.repository;

import com.getjavajob.mandzhievm.common.Account;
import com.getjavajob.mandzhievm.common.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query("select m from Message m where (m.from =?1 and m.to=?2) or (m.from=?2 and m.to=?1) and m.type =?3 order by m.idMessage desc")
    List<Message> findMessage(Account from, Account to, String type);

    List<Message> findAllByToAndType(Account to,String type);
}
