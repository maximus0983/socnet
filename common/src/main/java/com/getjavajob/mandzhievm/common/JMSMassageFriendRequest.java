package com.getjavajob.mandzhievm.common;


import java.io.Serializable;

public class JMSMassageFriendRequest implements Serializable {
    public static final String SEQUENCE_NAME = "usersSequence";

    private long id;
    private String emailFrom;
    private String emailTo;
    private String name;
    private String lastName;


    public JMSMassageFriendRequest() {
    }

    public JMSMassageFriendRequest(String emailFrom, String emailTo, String name, String lastName) {
        this.emailFrom = emailFrom;
        this.emailTo = emailTo;
        this.name = name;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "JMSMassageFriendRequest{" +
                "id=" + id +
                ", emailFrom='" + emailFrom + '\'' +
                ", emailTo='" + emailTo + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
