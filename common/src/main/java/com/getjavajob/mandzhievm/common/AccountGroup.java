package com.getjavajob.mandzhievm.common;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "accountgroup")
public class AccountGroup {
    @EmbeddedId
    private AccountGroupKey id;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id")
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "accountId")
    private Account account;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idGroup")
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "groupId")
    private Group group;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum('admin','user')")
    private User user;
    private int status;

    public AccountGroup() {
    }

    public AccountGroup(Account account, Group group) {
        this.account = account;
        this.group = group;
        this.id = new AccountGroupKey(account.getId(), group.getIdGroup());
    }

    public AccountGroupKey getId() {
        return id;
    }

    public void setId(AccountGroupKey id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Enum<User> getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountGroup that = (AccountGroup) o;

        if (!Objects.equals(id, that.id)) return false;
        if (!Objects.equals(account, that.account)) return false;
        if (!Objects.equals(group, that.group)) return false;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}