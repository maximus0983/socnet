package com.getjavajob.mandzhievm.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class FriendsKey implements Serializable {
    @Column(name = "acc_one_id")
    private int accOne;
    @Column(name = "acc_two_id")
    private int accTwo;

    public FriendsKey() {
    }

    public FriendsKey(int accountOne, int accountTwo) {
        this.accOne = accountOne;
        this.accTwo = accountTwo;
    }

    public int getAccountOne() {
        return accOne;
    }


    public int getAccountTwo() {
        return accTwo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendsKey that = (FriendsKey) o;
        return accOne == that.accOne &&
                accTwo == that.accTwo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accOne, accTwo);
    }
}
