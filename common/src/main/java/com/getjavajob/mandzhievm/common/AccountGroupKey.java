package com.getjavajob.mandzhievm.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AccountGroupKey implements Serializable {
    @Column(name = "accountId")
    private int id;
    @Column(name = "groupId")
    private int idGroup;

    private AccountGroupKey() {
    }

    public AccountGroupKey(int id, int idGroup) {
        this.id = id;
        this.idGroup = idGroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountGroupKey that = (AccountGroupKey) o;

        return id == that.id && idGroup == that.idGroup;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idGroup;
        return result;
    }
}
