package com.getjavajob.mandzhievm.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "community")
public class Group {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nameGroup;
    private LocalDate createDate;
    private byte[] photo;
    private String info;
    private int accountId;
    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AccountGroup> accounts = new ArrayList<>();

    public Group() {
    }

    public Group(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    public List<AccountGroup> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountGroup> accounts) {
        this.accounts = accounts;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getIdGroup() {
        return id;
    }

    public void setIdGroup(int idGroup) {
        this.id = idGroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (id != group.id) return false;
        return accountId == group.accountId && (nameGroup != null ? nameGroup.equals(group.nameGroup) : group.nameGroup == null)
                && (createDate != null ? createDate.equals(group.createDate) : group.createDate == null) &&
                Arrays.equals(photo, group.photo) && (info != null ? info.equals(group.info) : group.info == null) &&
                (accounts != null ? accounts.equals(group.accounts) : group.accounts == null);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nameGroup != null ? nameGroup.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(photo);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + accountId;
        result = 31 * result + (accounts != null ? accounts.hashCode() : 0);
        return result;
    }
}