package com.getjavajob.mandzhievm.common.repository;

import com.getjavajob.mandzhievm.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    Account findByEmail(String email);

    List<Account> findAllByFirstNameContainingOrLastNameContainingAllIgnoreCase(String firstName, String lastName, Pageable page);

    Long countByLastNameIsContainingOrFirstNameIsContainingAllIgnoreCase(String lastName, String firstName);

    Account findFirstById(Integer id);
}